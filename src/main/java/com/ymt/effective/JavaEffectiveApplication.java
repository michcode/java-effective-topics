package com.ymt.effective;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaEffectiveApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaEffectiveApplication.class, args);
	}
}
